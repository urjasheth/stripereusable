//
//  AppDelegate.swift
//  StripeReusable
//
//  Created by solulab on 3/7/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import MBProgressHUD
import Reachability
import Stripe
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let reachability = Reachability()!
    var customerContext: STPCustomerContext!
    var paymentContext: STPPaymentContext!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.sharedManager().enable = true

        // Override point for customization after application launch.
        let enableLocationViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShowCardsViewController") as! ShowCardsViewController
        let nav1 = UINavigationController(rootViewController: enableLocationViewObj)
        
        self.window?.rootViewController = nav1
        startSTPSession()
        return true
    }
    @objc func reachabilityChanged(note: NSNotification)
    {
        let reachability = note.object as! Reachability
        
        if reachability.connection != .none
        {
            if reachability.connection == .wifi
            {
                print("Reachable via WiFi")
            }
            else
            {
                print("Reachable via Cellular")
            }
        }
        else
        {
            print("Network not reachable")
        }
    }
//    func showHUD()
//    {
//        hideHUD()
//        MBProgressHUD.showAdded(to: (UIApplication.shared.keyWindow?.rootViewController?.view)!, animated: true)
//    }
//    
    func hideHUD()
    {
        MBProgressHUD.hide(for: (UIApplication.shared.keyWindow?.rootViewController?.view)!, animated: true)
    }
    func showAlert(message:String)
    {
        let alert = UIAlertController.init(title: APP_NAME, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .cancel, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    func startSTPSession() {
        let config = STPPaymentConfiguration.shared()
        config.publishableKey = StripePublishableKey
        
        customerContext = STPCustomerContext(keyProvider: MyAPIClient.sharedClient)
        paymentContext = STPPaymentContext(customerContext: customerContext,
                                           configuration: config,
                                           theme: STPTheme.init())
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
