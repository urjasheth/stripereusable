//
//  AddCardViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 10/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import Stripe
import MBProgressHUD

protocol ReloadCardDelegate {
   func reloadCards()
}

class AddCardViewController: UIViewController, UITextFieldDelegate,STPPaymentCardTextFieldDelegate {
   
   @IBOutlet var viewHeight : NSLayoutConstraint!
   @IBOutlet var lblAlert : UILabel!
   @IBOutlet var alertView : UIView!

   @IBOutlet var txtNameOnCard : UITextField!
   @IBOutlet var paymentTextField: STPPaymentCardTextField! = nil
   var submitButton: UIButton! = nil
   
   @IBOutlet var txtName: UITextField!
   @IBOutlet var txtAddress: UITextField!
   @IBOutlet var txtApt: UITextField!
   @IBOutlet var txtCity: UITextField!
   @IBOutlet var txtState: UITextField!
   @IBOutlet var txtZip: UITextField!
   
   @IBOutlet var lblNameOnCard: UILabel!
   @IBOutlet var lblAddress: UILabel!
   @IBOutlet var lblApt: UILabel!
   @IBOutlet var lblCity: UILabel!
   @IBOutlet var lblState: UILabel!
   @IBOutlet var lblZip: UILabel!

    var apiAdapter: STPBackendAPIAdapter!
   
   var delegate: ReloadCardDelegate!
   
   override func viewDidLoad()
   {
      super.viewDidLoad()
      
      self.title = "Add Payment"
      viewHeight.constant = self.view.frame.height - 64
      
      let button1 = UIButton(type: UIButtonType.system)
      button1.setTitle("Back", for: .normal)
      button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
      button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      button1.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)

      let barButton1 = UIBarButtonItem(customView: button1)
      self.navigationItem.leftBarButtonItem = barButton1
      alertView.isHidden = true
   }
   
   @IBAction func btnBackPress(sender:UIButton)
   {
      self.navigationController?.popViewController(animated: true)
   }
   
   override func didReceiveMemoryWarning()
   {
      super.didReceiveMemoryWarning()
   }
  
   func validate() -> Bool
   {
      alertView.isHidden = true
      if paymentTextField.cardNumber == nil
      {
         lblAlert.text = "Please enter card number"
         alertView.isHidden = false

         return false
      }
      else
      {
         if paymentTextField.expirationYear == 0 || paymentTextField.expirationMonth == 0
         {
            lblAlert.text = "Please enter expiration month/year"
            alertView.isHidden = false

            return false
         }
         else
         {
            if paymentTextField.cvc == nil
            {
               lblAlert.text = "Please enter CVC"
               alertView.isHidden = false

               return false
            }
            else
            {
               if txtNameOnCard.text?.count == 0
               {
                  lblNameOnCard.backgroundColor = UIColor.red
                  appDel.showAlert(message: "Please enter name on card.")
                  return false
               }
               else
               {
                  lblNameOnCard.backgroundColor = UIColor.lightGray

                  if txtAddress.text?.count == 0
                  {
                     lblAddress.backgroundColor = UIColor.red
                    appDel.showAlert(message: "Please enter proper address.")
                     return false
                  }
                  else
                  {
                     lblAddress.backgroundColor = UIColor.lightGray

                     if txtApt.text?.count == 0
                     {
                        appDel.showAlert(message: "Please enter appartment.")

                        lblApt.backgroundColor = UIColor.red

                        return false
                     }
                     else
                     {
                        lblApt.backgroundColor = UIColor.lightGray

                        if txtCity.text?.count == 0
                        {
                            appDel.showAlert(message: "Please enter city.")

                           lblCity.backgroundColor = UIColor.red


                           return false
                        }
                        else
                        {
                           lblCity.backgroundColor = UIColor.lightGray

                           if txtState.text?.count == 0
                           {
                            appDel.showAlert(message: "Please enter state.")

                              lblState.backgroundColor = UIColor.red


                              return false
                           }
                           else
                           {
                              lblState.backgroundColor = UIColor.lightGray

                              if txtZip.text?.count == 0
                              {
                                appDel.showAlert(message: "Please enter zip code.")

                                 lblZip.backgroundColor = UIColor.red

                                 return false
                              }
                              else
                              {
                                 lblZip.backgroundColor = UIColor.lightGray

                                 return true
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }
   @IBAction func addPaymentCard()
   {
      alertView.isHidden = true
      if validate()
      {
         let card = STPCardParams.init()
         card.number = paymentTextField.cardNumber!
         card.name = self.txtName.text!
         
         let address = STPAddress.init()
         address.line1 = self.txtAddress.text!
         address.line2 = self.txtApt.text!
         address.city = self.txtCity.text!
         address.state = self.txtState.text!
         address.postalCode = self.txtZip.text!
         
         card.address = address
         card.cvc = paymentTextField.cvc!

         card.expMonth = paymentTextField.expirationMonth
         card.expYear = paymentTextField.expirationYear
         
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let cardSouce = STPSourceParams.cardParams(withCard: card)
         
         STPAPIClient.shared().createSource(with: cardSouce) { (source, error) in
            if error == nil {
               appDel.customerContext.attachSource(toCustomer: source!, completion: { (error) in
                  
                  if error == nil
                  {
                     self.delegate.reloadCards()
                     self.navigationController?.popViewController(animated: true)
                    MBProgressHUD.hide(for: self.view, animated: true)
                  } else {
                    MBProgressHUD.hide(for: self.view, animated: true)

                     appDel.showAlert(message: "\(String(describing: error!.localizedDescription))")
                  }
               })
            } else {
                appDel.showAlert(message: "\(String(describing: error!.localizedDescription))")
                MBProgressHUD.hide(for: self.view, animated: true)
            }
         }
      }
   }
}
