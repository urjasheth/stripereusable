//
//  ShowCardsViewController.swift
//  StripeReusable
//
//  Created by solulab on 3/7/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import Stripe
import MBProgressHUD
@objc protocol STSelectedCardDelegate
{
    func setCard()
}
class ShowCardsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, ReloadCardDelegate
{

    @IBOutlet var tblObj : UITableView!
    @IBOutlet var tblHeight : NSLayoutConstraint!
    @IBOutlet var viewHeight : NSLayoutConstraint!
    var delegate: STSelectedCardDelegate?
    var selectedSource: STPSource?

    var isFromSetting: Bool = false
    
    override func viewDidLoad()
    {
        self.tabBarController?.tabBar.isHidden = true
        
        super.viewDidLoad()
        self.title = "Payment"
        viewHeight.constant = self.view.frame.height - 114
        
        self.reloadTableData()
        tblObj.tableFooterView = UIView()
        self.reloadCards()
    }
    
    func reloadTableData()
    {
        DispatchQueue.main.async {
            self.tblObj.reloadData()
            self.tblObj.layoutIfNeeded()
            self.tblHeight.constant = self.tblObj.contentSize.height
        }
    }
    
    func reloadCards() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        STPCustomerClass.shared.getCustomer { (error) in
            
            self.reloadTableData()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblObj.layer.removeAllAnimations()
        tblHeight.constant = tblObj.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.tblObj.layoutIfNeeded()
            self.updateViewConstraints()
        }
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if STPCustomerClass.shared.cardSources.count == 0 {
            return 1
        } else {
            return STPCustomerClass.shared.cardSources.count + 1
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if STPCustomerClass.shared.cardSources.count == 0 || indexPath.row == STPCustomerClass.shared.cardSources.count  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellCard", for: indexPath) as! AddCardCell
            cell.selectionStyle = .none
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! paymentCell
            cell.selectionStyle = .none
            
            let cardObj = STPCustomerClass.shared.cardSources[indexPath.row].cardDetails!
            
            cell.lblCard.text = cardObj.last4!
            
            cell.imgView.image = STPImageLibrary.brandImage(for: cardObj.brand)
    
            if indexPath.row == 0 {
                cell.lblDefault.isHidden = false
            } else {
                cell.lblDefault.isHidden = true
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]?
    {
        
        if editActionsForRowAt.row != STPCustomerClass.shared.cardSources.count
        {
            // Delete card from source
            let Delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
                tableView.setEditing(false, animated: true)
                MBProgressHUD.hide(for: self.view, animated: true)
                appDel.customerContext.detachSource(fromCustomer: STPCustomerClass.shared.cardSources[index.row], completion: { (error) in
                    if error == nil {
                        STPCustomerClass.shared.getCustomer(completion: { (error) in
                            if error == nil {
                                self.reloadTableData()
                            }
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                        })
                    } else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        
                    }
                })
                
                print("delete button tapped")
            }
            Delete.backgroundColor = YellowColor
            
            // Make card default while purchase
            let defaultObj = UITableViewRowAction(style: .normal, title: "Default") { action, index in
                tableView.setEditing(false, animated: true)
                MBProgressHUD.showAdded(to: self.view, animated: true)
                appDel.customerContext.selectDefaultCustomerSource(STPCustomerClass.shared.cardSources[index.row], completion: { (error) in
                    
                    if error == nil {
                        STPCustomerClass.shared.getCustomer(completion: { (error) in
                            if error == nil {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                self.reloadTableData()

                                
                            } else {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                
                            }
                        })
                    } else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                })
                
                print("default button tapped")
            }
            defaultObj.backgroundColor = redColor
            
            return [defaultObj,Delete]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row != STPCustomerClass.shared.cardSources.count
        {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if indexPath.row == STPCustomerClass.shared.cardSources.count
        {
            if (editingStyle == UITableViewCellEditingStyle.delete) {

            }
            if (editingStyle == UITableViewCellEditingStyle.delete) {
                
            }
        } else {
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == STPCustomerClass.shared.cardSources.count
        {
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
            viewObj.delegate = self
            self.navigationController?.pushViewController(viewObj, animated: true)
        } else {
            if isFromSetting == true {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                appDel.customerContext.selectDefaultCustomerSource(STPCustomerClass.shared.cardSources[indexPath.row], completion: { (error) in
                    
                    if error == nil {
                        STPCustomerClass.shared.getCustomer(completion: { (error) in
                            if error == nil {
                                self.reloadTableData()
                                MBProgressHUD.hide(for: self.view, animated: true)
                                self.delegate!.setCard()
                                self.navigationController?.popViewController(animated: true)
                            } else {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                
                            }
                        })
                    } else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                })
            } else {
                STPCustomerClass.shared.selectedSource = STPCustomerClass.shared.cardSources[indexPath.row]
            }
        }
    }
    func getDefaultCard()
    {
        if STPCustomerClass.shared.cardSources.count > 0 {
            if let stpSource = STPCustomerClass.shared.defaultSource
            {
                self.selectedSource = stpSource
                startPurchase()
            }
            
        } else {
            appDel.showAlert(message: "Add card first.")
        }
    }
    @IBAction func purchaseClick(sender:UIButton)
    {
        getDefaultCard()
    }
    func startPurchase()
    {
        if let stpSource = self.selectedSource
        {
            var paymentDict = JSONDictionary()
            paymentDict["amount"] = "1000" as AnyObject // $10.00 (remove ".") // ammount
            paymentDict["customerId"] = MyAPIClient.sharedClient.customerID as AnyObject
            paymentDict["source"] = stpSource.stripeID as AnyObject
            paymentDict["destination_account"] = "acct_1ByctHEIHSYBbXgA" as AnyObject  // merchent id
     
            print(paymentDict)
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            STPCardApiManager.shared.makePayment(parameter: paymentDict) { (paymentResponse, error) in
                
                if error == nil
                {
                    appDel.showAlert(message: "Purchase for $10 is successfull..!!")
                }
                else
                {
                    appDel.showAlert(message: "\(String(describing: error!.localizedDescription))")
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
        else
        {
            appDel.showAlert(message: "Please add or select card.")
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
class paymentCell : UITableViewCell
{
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lblCard: UILabel!
    @IBOutlet var lblDefault: UILabel!
}
class AddCardCell: UITableViewCell {
    
    
}
