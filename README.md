# StripeReusable

Installation using cocoapods:

pod 'Alamofire' // for API integration

pod 'MBProgressHUD', '~> 1.0.0' // for loading spinner

pod 'Stripe' // payment integration using stripe

Functionality :

	•	All cards listing
	•	Add card
	•	Delete card
	•	Make Default card
	•	Make payment

Go to ShowCardsViewController :

- Change "destination_account" value to your merchent account id
- Also add additional parameters needed when “create charge” API is implemented from backend.

Go to STPCardApiManager :

- change baseurl
- Go to MyAPIClient class
	-> change baseURLString
	-> change customerID

Go to Constant.swift :

- change StripePublishableKey with your stripe publish key.
- change StripeSecrateKey with your stripe secret key.
